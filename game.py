import random

import color


class Game:
    def __init__(self):
        """Things needed for the game"""
        self.good_pegs = []
        self.hint_pegs = []
        self.guessed_pegs = []

    def reset(self):
        self.good_pegs = []
        self.guessed_pegs = []
        self.hint_pegs = []

    def login(self):
        """Invites the player to login."""
        gamertag = input('Enter your Gamertag :')
        print('New game started ! Player {}, try guessing the hidden pegs.'.format(gamertag))
        print("Here's the list of color and the number it is associated with from which you can choose from:\n "
              "Red = 1, Blue = 2, Green = 3,\n Yellow = 4, Purple = 5, White = 6\n")
        print("Good color & good position = black peg.\n"
              "Good color BUT wrong position = white dot.")

    def next_player_turn(self):
        """The guess of the player during a turn"""
        guess = input('Select 4 different pegs by typing them this way : 1234 or 6155, etc.\n\n'
                      'Enter your guess ''here :')
        if len(guess) == 4 and guess.isdigit():
            for peg in guess:
                self.guessed_pegs.append(int(peg))
            print('Your guess is {}'.format(guess))

    # TODO: Fix the no duplicate way of generating the solution
    def generate_solution(self):
        """Generates the list of good pegs to be guessed by the player"""
        max_good_pegs = 4
        while len(self.good_pegs) < max_good_pegs:
            peg_color_number = list(map(int, color.Color))
            random.shuffle(peg_color_number)
            peg_color_number.pop(random.randrange(len(peg_color_number)))
            peg_color_number.pop(random.randrange(len(peg_color_number)))
            self.good_pegs = peg_color_number
            print(self.good_pegs)

    def analysis(self):
        """Validates the player guess in order for the computer to give him hints (black_peg, white_peg)."""
        white_peg = 0
        black_peg = 0
        for guessed_peg in self.guessed_pegs:
            for good_peg in self.good_pegs:
                if guessed_peg == good_peg and self.guessed_pegs.index(good_peg) == self.good_pegs.index(guessed_peg):
                    black_peg += 1
                elif guessed_peg == good_peg and self.guessed_pegs.index(good_peg) != self.good_pegs.index(guessed_peg):
                    white_peg += 1
        return print('You have {} black peg(s) and {} white peg(s)'.format(black_peg, white_peg))

    def end_game(self):
        """End-game message"""
        print('You won !')
        return None

    def play(self):
        """Plays the game"""
        self.reset()
        self.login()
        self.generate_solution()
        while self.good_pegs is not self.guessed_pegs:
            self.next_player_turn()
            self.analysis()
        else:
            return self.end_game()
